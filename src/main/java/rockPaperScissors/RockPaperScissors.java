package rockPaperScissors;

import java.util.Arrays;
import java.util.Random;

import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
    	while (true) {
    		System.out.printf("Let's play round %d%n", roundCounter);
    		
    		String playerChoice = getPlayerChoice();
    		String computerChoice = getComputerChoice();
    		
    		if (playerChoice.equals(computerChoice)) {
    			System.out.printf("Human chose %s, computer chose %s. It's a tie!%n", playerChoice, computerChoice);
    		}
    		else if (playerChoice.equals("rock")) {
    			if (computerChoice.equals("scissors")) {
    				System.out.printf("Human chose %s, computer chose %s. Computer wins!%n", playerChoice, computerChoice);
    				computerScore += 1;
    			}
    			else {
    				System.out.printf("Human chose %s, computer chose %s. Human wins!%n", playerChoice, computerChoice);
    				humanScore += 1;
    			}
    		}
    		else if (playerChoice.equals("scissors")) {
        		if (computerChoice.equals("paper")) {
        			System.out.printf("Human chose %s, computer chose %s. Human wins!%n", playerChoice, computerChoice);
        			humanScore += 1;
        		}
        		else {
        			System.out.printf("Human chose %s, computer chose %s. Computer wins!%n", playerChoice, computerChoice);
        			computerScore += 1;
        		}
    		}
    		else {
    			if (computerChoice.equals("rock")) {
    				System.out.printf("Human chose %s, computer chose %s. Human wins!%n", playerChoice, computerChoice);
    				humanScore += 1;
    			}
    			else {
    				System.out.printf("Human chose %s, computer chose %s. Computer wins!%n", playerChoice, computerChoice);
    				computerScore += 1;
    			}
    		}
    		System.out.printf("Score: human %d, computer %d%n", humanScore, computerScore);
    		String playAgain = readInput("Do you wish to continue playing? (y/n)?");
    		
    		if (playAgain.equals("n")) {
    			System.out.println("Bye bye :)");
    			break;
    		}
    		else {
    			roundCounter += 1;
    		}
    	}
    	
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    
    
    public String getComputerChoice() {
        Random random = new Random();
        String computerChoice = rpsChoices.get(random.nextInt(rpsChoices.size()));
        return computerChoice;
    }

    
    public String getPlayerChoice() {
    	while (true) {
    		String choice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
    		
			if (rpsChoices.contains(choice)) {
				return choice;
			}
			else {
				System.out.printf("I do not understand %s. Could you try again?", choice);
			}
    	}
    }
    
    
    
}